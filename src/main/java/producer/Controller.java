package producer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

@RestController
@Api(tags = {"marketplace"}, description = "Marketplace simulation")
public class Controller {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public static final String TOPIC_PRODUCT_ACTIVATION_RIO_CONNECT = "ccp.event.productactivationrequest.sku00000008";
    public static final String TOPIC_PRODUCT_ACTIVATION_RIO_CONNECT_ORDER = "ccp.event.productactivationrequest.sku00000009";
    public static final String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    @Autowired
    KafkaSender kafkaSender;


    @GetMapping(value = "/connect")
    public void connect(
            @ApiParam(value = "fleetId", required = true) @RequestParam String fleetId,
            @ApiParam(value = "quantity", required = true) @RequestParam int quantity,
            @ApiParam(value = "operation", required = true, allowableValues = "ACTIVATION,DEACTIVATION") @RequestParam String op) {
        kafkaSender.send(TOPIC_PRODUCT_ACTIVATION_RIO_CONNECT, getPayload(fleetId, "SKU00000008", op, quantity));
    }

    @GetMapping(value = "/order")
    public void order(
            @ApiParam(value = "fleetId", required = true) @RequestParam String fleetId,
            @ApiParam(value = "quantity", required = true) @RequestParam int quantity,
            @ApiParam(value = "operation", required = true, allowableValues = "ACTIVATION,DEACTIVATION") @RequestParam String op) {
        kafkaSender.send(TOPIC_PRODUCT_ACTIVATION_RIO_CONNECT_ORDER, getPayload(fleetId, "SKU00000009", op, quantity));
    }


    private String getPayload(String fleetId, String productId, String op, int quantity) {
        HashMap<String, String> payload = new HashMap<>();
        payload.put("quantity", String.valueOf(quantity));
        payload.put("transactionId", String.valueOf(new Date().getTime()));
        payload.put("fleetId", fleetId);
        payload.put("type", op);
        payload.put("productId", productId);
        payload.put("timestamp", new SimpleDateFormat(DATE_PATTERN).format(new Date()));
        StringBuilder sb = new StringBuilder("{");
        int i = 0;
        for (String s : payload.keySet()) {
            sb.append("\"");
            sb.append(s);
            sb.append("\":");
            sb.append("\"");
            sb.append(payload.get(s));
            sb.append("\"");
            i++;
            if (i < payload.keySet().size()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }








}
