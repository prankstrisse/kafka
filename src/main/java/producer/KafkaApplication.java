package producer;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.rioscania.platform.eventbus.consumer.Consumer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "producer")
@EnableSwagger2
public class KafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaApplication.class, args);
    }

    @PostConstruct
    public void startConsuming() {
        Gson gson = new Gson();
        Consumer rioStreamConsumer = new Consumer();

        rioStreamConsumer.subscribe(Arrays.asList("ccp.event.productactivationresponse"),
                record -> {
                    JsonElement jsonMessage = new JsonParser().parse((String) record.value());
                    System.out.println("Received message\n"
                            + "\t\tTopic: " + record.topic() + "\n"
                            + "\t\tPayload: " + jsonMessage);
                    rioStreamConsumer.commitSync();
                });
    }

    @Bean
    public Docket kafkaServiceRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("kafka")
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage(Controller.class.getPackage().getName()))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Marketplace simulation")
                .description("An API to buy and sell Connect and Order licenses")
                .termsOfServiceUrl("")
                .license("")
                .licenseUrl("")
                .version("1.0")
                .build();
    }

}
