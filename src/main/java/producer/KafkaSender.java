package producer;

import com.rioscania.platform.eventbus.topicmanager.KafkaTopicManager;
import com.rioscania.platform.eventbus.topicmanager.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.List;

@Service
public class KafkaSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaSender.class);

    private final KafkaTemplate<String, String> kafkaTemplate;

    public KafkaSender(@Autowired KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void send(String topic, String message) {
//        setupTopics(topic);

        // the KafkaTemplate provides asynchronous send methods returning a Future
        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topic, message);

        // register a callback with the listener to receive the result of the send asynchronously
        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

            @Override
            public void onSuccess(SendResult<String, String> result) {
                LOGGER.info("sent message='{}' with offset={}", message,
                        result.getRecordMetadata().offset());
            }

            @Override
            public void onFailure(Throwable ex) {
                LOGGER.error("unable to send message='{}'", message, ex);
            }
        });
    }

    public void setupTopics(String TOPIC) {
        KafkaTopicManager kafkaTopicManager = new KafkaTopicManager();
        List<Topic> listOfActiveTopics = kafkaTopicManager.getListOfActiveTopics();
        boolean topicExists = false;
        for (Topic topic : listOfActiveTopics) {
            if (TOPIC.equals(topic.getName())) {
                topicExists = true;
                break;
            }
        }
        if (!topicExists) {
            kafkaTopicManager.createTopic(new Topic(TOPIC, 1, 1));
            LOGGER.info("Create topic: " + TOPIC);
        } else {
            LOGGER.info("Topic: " + TOPIC + " exists.");
        }
    }
}